import React, { Component } from 'react';
import MostRecentSmallData from '../src/components/MostRecentSmallData';

export default class MostRecentSmall extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: null,
      active: 0,
      term: ''
    };

    this.loadData();
  }

  loadData() {
    fetch(this.props.data)
	  .then(response => { return response.json(); })
	  .then(agents => {
		  this.initialData = agents
		  this.setState({
			data: this.initialData
		  });
    });
  }

  updateData(config) {
    this.setState(config);
  }

  render() {
	if (!this.initialData) { return (<p>Loading...</p>); }
	const users = this.initialData.map((user, index) => {
		return (<MostRecentSmallData user={user} key={`user-${index}`} index={index} update={this.updateData.bind(this)} />);
	});
    return (
			<div className="box-images clearfix mostRecentSmall">
		{users}
										</div>

    );
  }
}
