import React, { Component } from 'react';
// import AgentList from '../src/components/AgentList';
// import load from '../src/utils/load';


export default class Contacts extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: null,
      active: 0,
      term: ''
    };

    // this.loadData();
  }

  loadData() {
    fetch(this.props.data)
	  .then(response => { return response.json(); })
	  .then(agents => {
		  this.initialData = agents
		  this.setState({
			data: this.initialData
		  });
    });
  }

  updateData(config) {
    this.setState(config);
  }

  render() {
    return (
							<div className="contactsContainer col-xs-12 col-sm-6 col-md-3">
        <div className="widget">
            <h3>Contact us</h3>

            <table className="contact">
                <tbody>
                    <tr>
                        <th className="address"><i className="fa fa-map-marker"></i> Address</th>
                        <td>5th Avenue<br/>New York, NYC 12345<br/>United States<br/></td>
                    </tr>

                    <tr>
                        <th className="phone"><i className="fa fa-phone"></i> Phone</th>
                        <td>+48 987 654 321</td>
                    </tr>

                    <tr>
                        <th className="email"><i className="fa fa-envelope"></i> E-mail</th>
                        <td><a href="mailto:info@example.com">info@example.com</a></td>
                    </tr>

                    <tr>
                        <th className="skype"><i className="fa fa-skype"></i>Skype</th>
                        <td>your.company</td>
                    </tr>

                    <tr>
                        <th className="gps"><i className="fa fa-compass"></i> GPS</th>
                        <td>31.21, -43.32</td>
                    </tr>
                </tbody>
            </table>
        </div>
							</div>
    );
  }
}
