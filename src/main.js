import React, { Component } from 'react';
import { browserHistory, Router } from 'react-router';
import { Provider, connect } from 'react-redux';
import routes from './routes';
// import configureStore from './redux/configureStore';

import { LocaleProvider } from 'antd';
import enUS from 'antd/lib/locale-provider/en_US';
import ruRU from 'antd/lib/locale-provider/ru_RU';

import appLocaleEN from './locale/en_US';
import appLocaleRU from './locale/ru_RU';
import Social from '../src/social';
import FeedBack from '../src/feedBack';
import Contacts from '../src/contacts';
import PropertyTypes from '../src/propertyTypes';
import RecentProperties from '../src/recentProperties';

import MostRecentSmall from '../src/MostRecentSmall';
import Professional from '../src/professional';
import Technology from '../src/technology';

import NavBar from '../src/navBar';

import MostRecentProperties from '../src/mostRecentProperties';
import Agents from '../src/agents';
import FeaturedProperties from '../src/FeaturedProperties';

import MapFilter from '../src/mapFilter';
import Register from '../src/Register';
import Locale from '../src/Locale';

class Main extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: null,
      active: 0,
      term: ''
    };

    this.loadData();
  }

  loadData() {
    fetch(this.props.data)
	  .then(response => { return response.json(); })
	  .then(agents => {
		  this.initialData = agents
		  this.setState({
			data: this.initialData
		  });
    });
  }

  updateData(config) {
    this.setState(config);
  }

  render() {
console.log('main ___ ', this.props.user.locale)
	let curLocale = enUS;
	let appLocale = appLocaleEN;
	if (this.props.user.locale === 'ru') {
		curLocale = ruRU;
		appLocale = appLocaleRU;
	}
    return (
		<LocaleProvider locale={curLocale}>
<div className="main">
    <div className="header-wrapper">
        <div className="header">
            <div className="header-inner">
                <nav className="navbar navbar-default">
                    <div className="header-secondary">
                        <div className="container clearfix">
                            <Locale />
                            <Register appMessages={appLocale} />
                        </div>
                    </div>

					<NavBar />

                </nav>
            </div>

        </div>

    </div>

    <div className="main-wrapper">
        <div className="main">
            <div className="main-inner">
				<div className="google-map-wrapper">
					<div className="google-map"></div>
					<MapFilter />
				</div>

				<div className="banner oveflow-hidden hidden-xs">
					<img src="assets/img/tmp/real-estate/banner2.png" alt="" className="h250"/>

					<div className="dark-background"></div>

					<div className="hero-text center">
						<div className="large">Real Estate Directory</div>
						<div className="small">Rock Solid HTML Template</div>
					</div>
				</div>
				<FeaturedProperties data="./assets/data/FeaturedProperties.json" />
				<Agents data="./assets/data/agents.json" />
				<MostRecentProperties data="./assets/data/mostRecentProperties.json" />
				<Technology />
				<Professional />

			</div>
			<MostRecentSmall data="./assets/data/mostRecentSmallProperties.json" />
		</div>
        </div>
    
    <div className="footer-wrapper">
        <div className="footer">
            <div className="footer-inner">
                <div className="footer-top">
					<div className="container">
						<div className="row">
							<RecentProperties data="./assets/data/recentProperties.json" />
							<PropertyTypes data="./assets/data/propertyTypesItems.json" />
							<Contacts />
							<FeedBack />
						</div>
						<Social />
					</div>
				</div>

                <div className="footer-bottom">
                    <div className="container">
                        <nav className="clearfix">
                            <ul className="nav navbar-nav footer-nav">
                                <li><a href="../../demo/automotive/index.html">Automotive</a></li>
                                <li><a href="../../demo/directory/index.html">Directory</a></li>
                                <li><a href="../../demo/real-estate/index.html">Real Estate</a></li>
                            </ul>

                        </nav>

                        <div className="copyright">
                            Designed by <a href="http://themeforest.com/user/themefire">ThemeFire</a> / Assembled in <a href="http://pragmaticmates.com">Pragmatic Mates</a>
                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>
</div>
		</LocaleProvider>
    );
  }
}
export default connect(
  (state, ownProps) => ({
    user: state.user,
    ownProps
  })
  ,
  dispatch => ({
    onAddTrack: (name) => {
      const payload = {
        id: Date.now().toString(),
        name
      };
      dispatch({ type: 'ADD_TRACK', payload });
    }
  })
)(Main);
