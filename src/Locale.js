import React, {PropTypes} from 'react';
import { connect } from 'react-redux';

import { Radio } from 'antd';
const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;

class Locale extends React.Component {
  constructor() {
    super();
    this.state = {
      locale: 'en',
    };
  }
  changeLocale = (e) => {
	const val = { locale: e.target.value };
// console.log('changeLocale ___ ', val)
	this.props.onSubmitChange(val);
    this.setState(val);
  }
  render() {
    return (
		<RadioGroup defaultValue="en" size="large" onChange={this.changeLocale}>
		  <RadioButton value="en"><img className="language-flag" src="css/img/au.png" alt="au" title="Australia"/></RadioButton>
		  <RadioButton value="ru"><img className="language-flag" src="css/img/ru.png" alt="ru" title="Russia"/></RadioButton>
		</RadioGroup>
    );
  }
}

export default connect(
  (store, ownProps) => ({
    user: store.user,
    ownProps
  }),
  dispatch => ({
    onSubmitChange: (obj) => {
	  const data = { id: Date.now().toString(), obj };
	  dispatch({ type: 'USER_LOCALE', data });
    }
  })
)(Locale);
