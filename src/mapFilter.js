import React, { Component } from 'react';
// import AgentList from '../src/components/AgentList';
// import load from '../src/utils/load';


export default class MapFilter extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: null,
      active: 0,
      term: ''
    };

    // this.loadData();
  }

  loadData() {
    fetch(this.props.data)
	  .then(response => { return response.json(); })
	  .then(agents => {
		  this.initialData = agents
		  this.setState({
			data: this.initialData
		  });
    });
  }

  updateData(config) {
    this.setState(config);
  }

  render() {
    return (
					<div className="mapFilterContainer container">
		<div className="mapFilter col-xs-12 col-sm-4 col-md-3 pull-right">
			<form method="post" action="?" className="clearfix">
				<div className="form-group">
					<input type="text" className="form-control" placeholder="Address" />
				</div>
			</form>
            <div className="form-group">
                <select name="" data-placeholder="Select Country" className="form-control">
                    <option value="#">Select Country</option>
                    <option value="#">Czech Republic</option>
                    <option value="#">Germany</option>
                    <option value="#">France</option>
                    <option value="#">Hungary</option>
                    <option value="#">Italy</option>
                    <option value="#">Poland</option>
                    <option value="#">Slovak Republic</option>
                    <option value="#">Ukraine</option>
                </select>
            </div>

            <div className="form-group">
                <select name="" data-placeholder="Select Disctrict" className="form-control">
                    <option value="#">Select Disctrict</option>
                    <option value="#">Brooklyn</option>
                    <option value="#">Bronx</option>
                    <option value="#">Manhattan</option>
                    <option value="#">Village</option>
                    <option value="#">Queens</option>
                </select>
            </div>

            <div className="form-group">
                <select name="" data-placeholder="Select City" className="form-control">
                    <option value="#">Select City</option>
                    <option value="#">Berlin</option>
                    <option value="#">Canberra</option>
                    <option value="#">London</option>
                    <option value="#">New York</option>
                    <option value="#">Paris</option>
                    <option value="#">Prague</option>
                </select>
            </div>

            <div className="form-group">
                <select name="" data-placeholder="Select Business Type" className="form-control">
                    <option value="#">Select Business Type</option>
                    <option value="#">Autoservice</option>
                    <option value="#">Bank</option>
                    <option value="#">Bar</option>
                    <option value="#">Castle</option>
                    <option value="#">Hospital</option>
                    <option value="#">Museum</option>
                    <option value="#">Restaurant</option>
                </select>
            </div>

            <div className="form-group">
                <div className="row">
                    <div className="col-xs-6 col-sm-6">
                        <input type="text" className="form-control" placeholder="Price From" />
                    </div>

                    <div className="col-xs-6 col-sm-6">
                        <input type="text" className="form-control" placeholder="Price To" />
                    </div>
                </div>
            </div>

            <div className="form-group">
                <input type="submit" value="Search" className="btn btn-terciary btn-block" />
            </div>
        </div>
					</div>
    );
  }
}
