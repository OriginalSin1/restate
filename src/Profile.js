import React, {PropTypes} from 'react';
import { connect } from 'react-redux';
import Dialog from 'rc-dialog';

// import 'rc-cropping/assets/index.less';
import CropViewer from 'rc-cropping';

import Register from '../src/Register';

class Profile extends React.Component {
  constructor(props) {
    super(props);

    let uHash = props.user.uHash;
	this.state = {
		pageType: 'Preview',
		editProfile: false,
		fName: uHash.fName || '',
		sName: uHash.sName || '',
		about: uHash.about || '',
		isShowingModal: false
    };
    // this.loadData();
  }
  onSubmit = (e) => {
	let flag = true,
		out = {pageType: 'Preview'};
	[... e.target.form.elements].forEach((it) => {
		if (it.name) {
			out[it.name] = it.value;
		}
	});
	this.setState(out);
	this.props.onSubmitChange(out);
	return;
  }
  onChange = (e) => {
	  let target = e.target,
		key = target.name,
		value = target.value || '',
		flag = true,
		out = {};

	  if (target.required) {
		target.className = target.className.replace(/ error/g, '');
		let len = value.length,
			it = this.getPropsByName(key),
			reg = it.type === 'email' ? /^[-._a-z0-9]+@(?:[a-z0-9][-a-z0-9]+\.)+[a-z]{2,6}$/ : /^[\w]+$/;
		if (
			(key === 'cPwd' && value !== this.state.oPwd) ||
			value.search(reg) === -1 ||
			(len < it.minSize || len > it.maxSize)
			// (flag && (len < it.minSize || len > it.maxSize))
			) {
			target.className += ' error';
			flag = false;
		}
	  }
	  // if (key !== 'uPwd' && key !== 'cPwd') {
		  out[key] = value;
	  // }
	  this.setState(out);
	  return flag;
  }

  handleClose = () => {
	  this.setState({isShowingModal: false, editProfile: false});
	  this.props.onSubmitChange(this.state);
  }

  handleFirstName = () => {
	  this.setState({pageType: 'FirstName'});
	  // this.props.onSubmitChange(this.state);
  }

  handleChangePass = () => {
	  // this.setState({isShowingModal: false, editProfile: false, pageType: 'ChangePassword'});
	  this.props.handleChangePass();
  }

  handleChangeEmail = () => {
	  this.props.handleChangeEmail();
  }
  fields = [
	  {id: 0, name: 'fName', label: 'First Name', maxSize: 128, type: 'text', value: '', placeholder: 'John'},
	  {id: 1, name: 'sName', label: 'Surname', maxSize: 128, type: 'text', value: '', placeholder: 'Doe'},
	  {id: 2, name: 'about', label: 'About', type: 'textarea', value: '', placeholder: 'About'},
	  {id: 3, type: 'button', onClick: this.onSubmit, value: 'Update', className: 'btn btn-primary'},
	  {id: 4, type: 'button', onClick: this.handleChangePass, value: 'Change Password', className: 'rc-btn rc-btn-ghost', classNameParent: ''},
	  {id: 5, type: 'button', onClick: this.handleChangeEmail, value: 'Change Email', className: 'rc-btn rc-btn-ghost', classNameParent: ''},

	  {id: 3, name: 'uLogin', label: 'Login Name', required: true, minSize: 1, maxSize: 16, type: 'text', value: '', title: 'Must be 1-16 characters, only letters and numbers (a-z, A-Z, 0-9)', placeholder: 'MyLogin'},
	  {id: 4, name: 'uPwd', label: 'Password', required: true, minSize: 4, maxSize: 64, type: 'password', value: '', title: 'Must be 8-64 characters, only letters and numbers (a-z, A-Z, 0-9)', placeholder: 'mypass1234'},
	  {id: 5, name: 'oPwd', label: 'New Password', required: true, minSize: 4, maxSize: 64, type: 'password', value: '', title: 'Must be 8-64 characters, only letters and numbers (a-z, A-Z, 0-9)', placeholder: 'mypass1234'},
	  {id: 6, name: 'cPwd', label: 'Confirm Password', required: true, required: true, type: 'password', value: '', title: 'must be equal topreviousle field', placeholder: 'mypass1234'},
	  {id: 7, type: 'button', onClick: this.onSubmit, value: 'Register', className: 'btn btn-primary'},
	  {id: 8, type: 'button', onClick: this.onSubmit, value: 'Login', className: 'btn btn-primary'},
	  {id: 9, type: 'button', onClick: this.onSubmit, value: 'Send Password', className: 'btn btn-primary'},
	  {id: 10, type: 'button', onClick: this.onSubmit, value: 'Forgot your Password?', className: 'btn btn-info', classNameParent: ''},
	  {id: 12, name: 'nEmail', label: 'New E-mail', required: true, maxSize: 128, type: 'email', value: '', title: 'You will need to verify your email address to complete the registration', placeholder: 'xxx@domain.zone'},
  ]
  renderItem(index): ?React.Element<any> {
	let it = this.fields[index];
	let label = it.label;
	let name = it.name;
	let type = it.type;
	let nodeName = it.type === 'textarea' ? 'textarea' : 'input';
	let value = this.state[name] || it.value;
	let classNameParent = 'form-group ' + (it.classNameParent || '');
    return (
		<div className={classNameParent} key={index}>
			{ label &&
				<label>{it.label}
					{it.required ? <span className="required">(*)</span> : null}
				</label>
			}
			{ it.type && it.type === 'textarea' &&
				<textarea
					type={type}
					name={name}
					className={it.className || 'form-control'}
					value={value}
					title={it.title || null}
					placeholder={it.placeholder || null}
					onChange={this.onChange}
					onClick={it.onClick || null}
					required={it.required ? true : null}
				/>
			}
			{ it.type && it.type !== 'textarea' &&
				<input
					type={type}
					name={name}
					className={it.className || 'form-control'}
					value={value}
					title={it.title || null}
					placeholder={it.placeholder || null}
					onChange={this.onChange}
					onClick={it.onClick || null}
					required={it.required ? true : null}
				/>
			}
		</div>
    );
  }
  render() {
console.log('render_______', this.state, this.props)
    let pageType = this.state.pageType || 'Preview';
	let about = this.state.about || 'About me';
	let photo = this.state.photo || this.props.user.uID;
	let imgUrl = 'assets/img/tmp/authors/' + photo + '-large.jpg';
	let wrapClassName = 'center';
	let validFields = [0, 1, 2, 3, 4];
    return (
		<div className="editProfile">
		{
			pageType === 'Preview' &&
			<div className="box background-white">
				<div className="row">
					<div className="col-md-12 col-lg-5">
						<div className="row">
							<div className="box-picture" title="Add new photo">
								<a><img src={imgUrl} alt="Your photo" />
								</a>
							</div>
								<div className="box-crop">
			<CropViewer
			  getSpinContent={() => <span>loading...</span> }
			  renderModal={() => <Dialog />}
			  circle={true}
			/>
								</div>
						</div>
					</div>
					<div className="col-md-12 col-lg-7">
						<div className="row">
							<div onClick={this.handleFirstName.bind(this)} className="box-body" title="Edit this block">
								<h2 className="box-title-plain"><a>{this.state.fName || 'John'} {this.state.sName || 'Smith'}</a></h2>
								<p>{about}</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		}
		{
			pageType === 'FirstName' &&
			<form>
				{validFields.map(this.renderItem.bind(this))}
			</form>
		}
		</div>
	)
  }
}

export default connect(
  (store, ownProps) => ({
    user: store.user,
    ownProps
  })
  ,
  dispatch => ({
    onSubmitChange: (obj) => {
	  const data = { id: Date.now().toString(), obj };
	  dispatch({ type: 'PROFILE_SUBMIT', data });
    }
  })
)(Profile);
