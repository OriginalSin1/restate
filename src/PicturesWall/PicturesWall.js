import React, {PropTypes} from 'react';
import { connect } from 'react-redux';
import { Upload, Icon, Modal } from 'antd';

const prefix = '//www.maphomes.ru/';

class PicturesWall extends React.Component {
  constructor(props) {
    super(props);
	let uHash = props.user.uHash;
	let img = prefix + 'images/' + props.user.uID + '_';
    this.state = {
		previewVisible: false,
		previewImage: '',
		prefixImage: img,
		fileList: [
			{
			  uid: -1,
			  name: 'xxx.png',
			  status: 'done',
			  url: uHash.img1 ? img + '1.jpg' : null
			},
			{
			  uid: -2,
			  // name: 'xxx.png',
			  status: 'done',
			  url: uHash.img2 ? img + '2.jpg' : null
			},
			{
			  uid: -3,
			  name: 'xxx.png',
			  status: 'done',
			  url: uHash.img3 ? img + '3.jpg' : null
			}
		]
	  };
  }

  handleRemove = (file) => {
	  let arr = this.state.fileList;
	  let ind = arr.indexOf(file);
	  if (ind > -1) {
		this.props.reqUser({delImg: ind + 1, uProfile: 1});
		arr[ind].url = null;
		this.setState({
		  fileList: arr
		});
	  }
  }

  handleCancel = () => this.setState({ previewVisible: false })

  handlePreview = (file) => {
    this.setState({
      previewImage: file.url || file.thumbUrl,
      previewVisible: true,
    });
  }

  handleChange = (num, {file}) => {
		let arr = this.state.fileList;
		arr[num] = file;
		// arr[num].status = 'done';
		// arr[num].url = this.state.prefixImage + (num + 1) + '.jpg';
		this.setState({
		  fileList: arr
		});
  }

  render() {
    const { previewVisible, previewImage, fileList } = this.state;
    return (
      <div className="clearfix">
		{fileList.map((it, index) => {
			let ind = index + 1;
			let ind1 = ind * 100;
			let fileItem = it.url === null ? [] : [it];
			let data = {cnt: ind, uProfile: 1};
			let onChange = (file) => {this.handleChange(index, file);}
			return (
      <div className="floatfix" key={index}>
		<Upload
          action = {prefix + 'cgi/auth.pl'}
          listType="picture-card"
          fileList={fileItem}
		  withCredentials={true}
          data={data}
          onPreview={this.handlePreview}
          onRemove={this.handleRemove}
          onChange={onChange}
        >
          {it.url === null &&
			  <span key={ind1}>
				<Icon type="plus" />
				<div className="ant-upload-text">{ind} picture</div>
			  </span>
		  }
        </Upload>
      </div>
		);
		})}
        <Modal visible={previewVisible} footer={null} onCancel={this.handleCancel}>
          <img alt="example" style={{ width: '100%' }} src={previewImage} />
        </Modal>
      </div>
    );
  }
}

export default connect(
  (store, ownProps) => ({
    user: store.user,
    ownProps
  }),
  dispatch => ({
    onSubmitChange: (obj) => {
	  const data = { id: Date.now().toString(), obj };
	  dispatch({ type: 'USER_SUBMIT', data });
    }
  })
)(PicturesWall);
