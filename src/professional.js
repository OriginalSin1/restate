import React, { Component } from 'react';
// import AgentList from '../src/components/AgentList';
// import load from '../src/utils/load';


export default class Professional extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: null,
      active: 0,
      term: ''
    };

    // this.loadData();
  }

  loadData() {
    fetch(this.props.data)
	  .then(response => { return response.json(); })
	  .then(agents => {
		  this.initialData = agents
		  this.setState({
			data: this.initialData
		  });
    });
  }

  updateData(config) {
    this.setState(config);
  }

  render() {
    return (
				<div className="professionalContainer block-content background-map background-white fullwidth">
		<div className="block-content-inner">
			<div className="row">
				<div className="col-sm-3 center">
					<i className="large-icon color-primary entypo paper-plane"></i>
					<h4>Unique Design</h4>

					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt
						ut laoreet dolore magna
					</p>
				</div>
				<div className="col-sm-3 center">
					<i className="large-icon color-primary entypo archive"></i>
					<h4>Perfect Files</h4>

					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt
						ut laoreet dolore magna
					</p>
				</div>
				<div className="col-sm-3 center">
					<i className="large-icon color-primary entypo star"></i>
					<h4>Creative Layout</h4>

					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt
						ut laoreet dolore magna
					</p>
				</div>
				<div className="col-sm-3 center">
					<i className="large-icon color-primary entypo lifebuoy"></i>
					<h4>Professional Support</h4>

					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt
						ut laoreet dolore magna
					</p>
				</div>
            </div>
        </div>
				</div>
    );
  }
}
