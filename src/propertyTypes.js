import React, { Component } from 'react';
import PropertyTypesList from '../src/components/PropertyTypesList';
// import load from '../src/utils/load';


export default class PropertyTypes extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: null,
      active: 0,
      term: ''
    };

    this.loadData();
  }

  loadData() {
    fetch(this.props.data)
	  .then(response => { return response.json(); })
	  .then(agents => {
		  this.initialData = agents
		  this.setState({
			data: this.initialData
		  });
    });
  }

  updateData(config) {
    this.setState(config);
  }

  render() {
    return (
							<div className="propertyTypes col-xs-12 col-sm-6 col-md-3">
		<div className="widget">
            <h3>Property Types</h3>
			<PropertyTypesList data={this.state.data} update={this.updateData.bind(this)} />
        </div>
							</div>
    );
  }
}
