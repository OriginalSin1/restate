import React, { Component } from 'react';
import AgentList from '../src/components/AgentList';
// import load from '../src/utils/load';


export default class Agents extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: null,
      active: 0,
      term: ''
    };

    this.loadData();
  }

  loadData() {
    fetch(this.props.data)
	  .then(response => { return response.json(); })
	  .then(agents => {
		  this.initialData = agents
		  this.setState({
			data: this.initialData
		  });
    });
  }

  updateData(config) {
    this.setState(config);
  }

  render() {
    return (
				<div className="container agents">
		<div className="block-content fullwidth background-gray">
            <AgentList data={this.state.data} update={this.updateData.bind(this)} />
        </div>
				</div>
    );
  }
}
