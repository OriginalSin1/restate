import React, {PropTypes} from 'react';
import { connect } from 'react-redux';

import { Button, Modal, Icon, Form, Input, Tooltip, Radio, Checkbox } from 'antd';
import PicturesWall from '../src/PicturesWall/PicturesWall';

const FormItem = Form.Item;

const CollectionCreateForm = Form.create()(
  (props) => {
    const { visible, onCancel, onCreate, form, appMessages, user, reqUser } = props;
    const { getFieldDecorator } = form;
	const pattern = /^[\w]+$/;
	const rules = [{ required: true, max: 120, message: appMessages.errors.w, pattern: pattern }];
	const icon = <Icon type="user" style={{ fontSize: 13 }} />;
    // const formItemLayout = {
      // labelCol: { span: 6 },
      // wrapperCol: { span: 14 },
    // };
    const uHash = user.uHash || {};

    return (
      <Modal
        visible={visible}
        title={appMessages.profile}
        okText={appMessages.save}
        onCancel={onCancel}
        onOk={onCreate}
      >
		<Form className="login-form">
			<FormItem hasFeedback>
			  {getFieldDecorator('fName', { rules: rules,  initialValue: uHash.fName || '' })(
				<Input placeholder={appMessages.inputs.fName} prefix={icon} />
			  )}
			</FormItem>
			<FormItem hasFeedback>
			  {getFieldDecorator('sName', { rules: rules,  initialValue: uHash.sName || '' })(
				<Input placeholder={appMessages.inputs.sName} prefix={icon} />
			  )}
			</FormItem>
			<FormItem hasFeedback>
			  {getFieldDecorator('about', { initialValue: uHash.about || '' })(
				<Input type="textarea" rows={8} placeholder={appMessages.inputs.about} />
			  )}
			</FormItem>
			<PicturesWall fileList={uHash.images} reqUser={reqUser} />
        </Form>
      </Modal>
    );
  }
);

class ProfileForm extends React.Component {
  state = {
    visible: false,
  };
  showModal = () => {
    if (this.props.onCancel) { this.props.onCancel(); }
    this.setState({ visible: true });
  }
  handleCancel = () => {
    this.setState({ visible: false });
  }
  handleCreate = () => {
    const form = this.form;
    form.validateFields((err, values) => {
      if (err) {
        return;
      }
      // console.log('Received values of form: ', values);
	  values.uProfile = 1;
	  this.props.onSubmit(values);
      form.resetFields();
      this.setState({ visible: false });
    });
  }
  saveFormRef = (form) => {
    this.form = form;
  }
  render() {
	let appMessages = this.props.appMessages;
	let className = this.props.className || 'login-form-forgot';
	let divFlag = className === 'fa fa-users';
    return (
<span>
	<div onClick={this.showModal} className="mail-User-Picture js-user-picture is-updated" title="Edit profile"></div>
	<CollectionCreateForm
	  ref={this.saveFormRef}
	  user={this.props.user}
	  reqUser={this.props.reqUser}
	  appMessages={appMessages}
	  visible={this.state.visible}
	  onCancel={this.handleCancel}
	  onCreate={this.handleCreate}
	/>
</span>
    );
  }
}

export default connect(
  (store, ownProps) => ({
    user: store.user,
    ownProps
  }),
  dispatch => ({
    onSubmitChange: (obj) => {
	  const data = { id: Date.now().toString(), obj };
	  dispatch({ type: 'USER_SUBMIT', data });
    }
  })
)(ProfileForm);
