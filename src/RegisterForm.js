import React, {PropTypes} from 'react';
import { connect } from 'react-redux';

import { Button, Modal, Icon, Form, Input, Tooltip, Radio, Checkbox } from 'antd';
const FormItem = Form.Item;

const CollectionCreateForm = Form.create()(
  (props) => {
    const { visible, onCancel, onCreate, form, appMessages } = props;
    const { getFieldDecorator } = form;
	const pattern = /^[\w]+$/;
    const formItemLayout = {
      // labelCol: {
        // xs: { span: 24 },
        // sm: { span: 0 },
      // },
      // wrapperCol: {
        // xs: { span: 24 },
        // sm: { span: 0 },
      // },
    };
    return (
      <Modal
        visible={visible}
        title={appMessages.register}
        okText={appMessages.register}
        onCancel={onCancel}
        onOk={onCreate}
      >
		<Form className="login-form">
			<FormItem hasFeedback>
			  {getFieldDecorator('uEmail', {
				rules: [{ type: 'email', max: 120, required: true, message: appMessages.errors.email }],
			  })(
				<Input prefix={<Icon type="mail" style={{ fontSize: 13 }} />} placeholder={appMessages.inputs.uEmail} />
			  )}
			</FormItem>
			<FormItem hasFeedback>
			  {getFieldDecorator('uLogin', {
				rules: [{ required: true, max: 120, message: appMessages.errors.w, pattern: pattern }],
			  })(
				<Input prefix={<Icon type="user" style={{ fontSize: 13 }} />} placeholder={appMessages.inputs.uLogin} />
			  )}
			</FormItem>
			<FormItem hasFeedback>
			  {getFieldDecorator('uPwd', {
				rules: [
					{ max: 10, message: appMessages.errors.len },
					{ required: true, pattern: pattern, message: appMessages.errors.w }
				],
			  })(
				<Input prefix={<Icon type="lock" style={{ fontSize: 13 }} />} type="password" placeholder="Password"
				/>
			  )}
			</FormItem>
        </Form>
      </Modal>
    );
  }
);

class RegisterForm extends React.Component {
  state = {
    visible: false,
  };
  showModal = () => {
    if (this.props.onCancel) { this.props.onCancel(); }
    this.setState({ visible: true });
  }
  handleCancel = () => {
    this.setState({ visible: false });
  }
  handleCreate = () => {
    const form = this.form;
    form.validateFields((err, values) => {
      if (err) {
        return;
      }
      // console.log('Received values of form: ', values);
	  values.uForgot = 1;
	  this.props.onSubmit(values);
      form.resetFields();
      this.setState({ visible: false });
    });
  }
  saveFormRef = (form) => {
    this.form = form;
  }
  render() {
	let appMessages = this.props.appMessages;
	let className = this.props.className || 'login-form-forgot';
	let divFlag = className === 'fa fa-users';
    return (
		<span>
		{ divFlag &&
			<div className="fa fa-users" onClick={this.showModal}>
				<span>{appMessages.register}</span>
			</div>
		}
		{ !divFlag &&
		<span className={className} onClick={this.showModal}>
			{appMessages.registerNow}
		</span>
		}
			<CollectionCreateForm
			  ref={this.saveFormRef}
			  appMessages={appMessages}
			  visible={this.state.visible}
			  onCancel={this.handleCancel}
			  onCreate={this.handleCreate}
			/>
		</span>
    );
  }
}

export default connect(
  (store, ownProps) => ({
    user: store.user,
    ownProps
  }),
  dispatch => ({
    onSubmitChange: (obj) => {
	  const data = { id: Date.now().toString(), obj };
	  dispatch({ type: 'USER_SUBMIT', data });
    }
  })
)(RegisterForm);
