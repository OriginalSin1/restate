var locale = {
  uLogin: 'Имя пользователя',
  uPwd: 'Пароль',
  selectLang: 'Выбор языка',
  register: 'Регистрация',
  registerNow: 'Регистрация',
  forgot: 'Забытый пароль',
  login: 'Авторизация',
  logout: 'Выйти',
  log_in: 'Войти',
  send: 'Прислать',
  save: 'Сохранить',
  profile: 'Ваш профиль',
  placeholder: 'Выберите время',
  inputs: {
	uLogin: 'Пожалуйста введите имя пользователя!',
	uEmail: 'Введите свой E-mail!',
	fName: 'Имя',
	sName: 'Фамилия',
	about: 'О вас',
  },
  errors: {
	0: '',
	2: 'Пользователь не найден',
	3: 'Пароль не верен',
	len: 'Превышен максимальный размер поля',
	email: 'Неверный формат E-mail!',
	w: 'Только цифры и буквы',
  }
};
export default locale;