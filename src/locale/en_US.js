var locale = {
  uLogin: 'Username',
  uPwd: 'Password',
  selectLang: 'Select Language',
  register: 'Register',
  registerNow: 'Register now!',
  forgot: 'Forgot password',
  login: 'Login',
  logout: 'Logout',
  log_in: 'Log in',
  send: 'Send',
  save: 'Save',
  profile: 'Your profile',
  placeholder: 'Chose time',
  inputs: {
	uLogin: 'Please input your nickname!',
	uEmail: 'Please input your E-mail!',
	fName: 'First Name',
	sName: 'Surname',
	about: 'About',
  },
  errors: {
	0: '',
	2: 'User not found',
	3: 'Bad passowrd',
	len: 'Riched max size',
	email: 'The input is not valid E-mail!',
	w: 'Should be combination of numbers & alphabets'
  }
};
export default locale;