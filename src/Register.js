import React, {PropTypes} from 'react';
import { connect } from 'react-redux';
import Dialog from 'rc-dialog';
// import Cookies from 'universal-cookie';

import Profile from '../src/Profile';
import Login from '../src/Login';
import RegisterForm from '../src/RegisterForm';
import ProfileForm from '../src/ProfileForm';

class Register extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
		pageType: 'Register',
		uLogin: self.localStorage.getItem('uLogin') || null,
		fName: self.localStorage.getItem('fName') || null,
		sName: self.localStorage.getItem('sName') || null,
		// tmpSess1: new Cookies().getAll() || false,
		// tmpSess: new Cookies().get('tmpSess') || false,
		isShowingModal: false
    };

	this.reqUser({uAttr: 1});

    // this.loadData();
  }
  onChange = (e) => {
	  let target = e.target,
		key = target.name,
		value = target.value || '',
		flag = true,
		out = {};

	  if (target.required) {
		target.className = target.className.replace(/ error/g, '');
		let len = value.length,
			it = this.getPropsByName(key),
			reg = it.type === 'email' ? /^[-._a-z0-9]+@(?:[a-z0-9][-a-z0-9]+\.)+[a-z]{2,6}$/ : /^[\w]+$/;
		if (
			(key === 'cPwd' && value !== this.state.oPwd) ||
			value.search(reg) === -1 ||
			(len < it.minSize || len > it.maxSize)
			// (flag && (len < it.minSize || len > it.maxSize))
			) {
			target.className += ' error';
			flag = false;
		}
	  }
	  // if (key !== 'uPwd' && key !== 'cPwd') {
		  out[key] = value;
	  // }
	  this.setState(out);
	  return flag;
  }

  toStorageFields = ['uLogin', 'fName', 'sName', 'tmpSess']

  postProfile = (data) => {
	this.reqUser(data, {
		method: 'POST',
		mode: 'cors',
		credentials: 'include'
	});
  }

  reqUser = (data, options, script) => {
	let locHash = document.location;

	let prefix = locHash.hostname === 'localhost' ? 'http://www.maphomes.ru' : locHash.origin;
	let _this = this;
	options = options || {
		mode: 'cors',
		credentials: 'include'
	};
	script = script || 'auth.pl';

	mapUtils.requestJSON(prefix + '/cgi/' + script, data, options).then(function(response) {
		return response.json();
	}).then(function(json) {
// console.log('requestJSON', json)
		if (json && json.AUTH) {	// && json.AUTH.err === 0) {
			let tItems = json.AUTH;
			if (json.AUTH.tmpSess) {
				tItems.isShowingModal = false;
			}
			tItems.uPwd = tItems.cPwd = tItems.uEmail = '';
			if (tItems.pageType === 'Login') {
				tItems.err = -2;
			}
			_this.setState(tItems);
			_this.props.onSubmitChange(tItems);
		}
	});

  }
  onSubmit = (e) => {
	let flag = true,
		out = {};
	[... e.target.form.elements].forEach((it) => {
		if (it.name) {
			out[it.name] = it.value;
		}
		if (it.required && flag) {
			flag = this.onChange({target: it});
		}
	});
	if (flag) {
		out.uAttr = 1;
		out.tmpSess = this.state.tmpSess;
		if (this.state.pageType === 'Renew') {
			out.uForgot = 1;
		}
		if (this.state.pageType === 'ChangePassword') {
			out.uLogin = this.state.uLogin;
		}
		
		this.reqUser(out);
		// let data = Object.assign({uAttr: 1}, this.state, this.state.pageType === 'Renew' ? {uForgot: 1} : {});
		// this.reqUser(data);
		this.toStorageFields.forEach((it) => {
			if (this.state[it]) {
				self.localStorage.setItem(it, this.state[it]);
			}
		});
		if (out.uForgot) {
			this.setState({cmd: 'passSendEmail'});
		}
		this.props.onSubmitChange(this.state);
	}
	return flag;
  }
  handleRenew = () => this.setState({isShowingModal: true, pageType: 'Renew'})
  getPropsByName = (name) => {
	  let arr = this.fields;
	  for (let i = 0, len = arr.length; i < len; i++) {
		  if (arr[i].name === name) { return arr[i]; }
	  }
	  return null;
  }
  fields = [
	  {id: 0, name: 'uEmail', label: 'E-mail', required: true, maxSize: 128, type: 'email', value: '', title: 'You will need to verify your email address to complete the registration', placeholder: 'xxx@domain.zone'},
	  {id: 1, name: 'fName', label: 'First Name', maxSize: 128, type: 'text', value: '', placeholder: 'John'},
	  {id: 2, name: 'sName', label: 'Surname', maxSize: 128, type: 'text', value: '', placeholder: 'Doe'},
	  {id: 3, name: 'uLogin', label: 'Login Name', required: true, minSize: 1, maxSize: 16, type: 'text', value: '', title: 'Must be 1-16 characters, only letters and numbers (a-z, A-Z, 0-9)', placeholder: 'MyLogin'},
	  {id: 4, name: 'uPwd', label: 'Password', required: true, minSize: 4, maxSize: 64, type: 'password', value: '', title: 'Must be 8-64 characters, only letters and numbers (a-z, A-Z, 0-9)', placeholder: 'mypass1234'},
	  {id: 5, name: 'oPwd', label: 'New Password', required: true, minSize: 4, maxSize: 64, type: 'password', value: '', title: 'Must be 8-64 characters, only letters and numbers (a-z, A-Z, 0-9)', placeholder: 'mypass1234'},
	  {id: 6, name: 'cPwd', label: 'Confirm Password', required: true, required: true, type: 'password', value: '', title: 'must be equal topreviousle field', placeholder: 'mypass1234'},
	  {id: 7, type: 'button', onClick: this.onSubmit, value: 'Register', className: 'btn btn-primary'},
	  {id: 8, type: 'button', onClick: this.onSubmit, value: 'Login', className: 'btn btn-primary'},
	  {id: 9, type: 'button', onClick: this.onSubmit, value: 'Send Password', className: 'btn btn-primary'},
	  {id: 10, type: 'button', onClick: this.onSubmit, value: 'Forgot your Password?', className: 'btn btn-info', classNameParent: ''},
	  {id: 11, type: 'button', onClick: this.onSubmit, value: 'Change Password', className: 'btn btn-primary', classNameParent: ''},
	  {id: 12, name: 'nEmail', label: 'New E-mail', required: true, maxSize: 128, type: 'email', value: '', title: 'You will need to verify your email address to complete the registration', placeholder: 'xxx@domain.zone'},
	  {id: 13, type: 'button', onClick: this.onSubmit, value: 'Change Email', className: 'btn btn-primary', classNameParent: ''}
  ]
  fieldsType = {
	  Register: { items: [0, 1, 2, 3, 4, 6, 7] },
	  ChangePassword: { title: 'Change Password', items: [4, 5, 6, 11] },
	  ChangeEmail: { title: 'Change Email', items: [0, 12, 13] },
	  Renew: { title: 'Renew Password', items: [0, 9] },
	  Login: { items: [3, 4, 8, 10] }
  }
  getValidFields = () => {
	let type = this.state.pageType;
	return this.fieldsType[type].items.map((num) => {
		return this.fields[num];
	});
  }
  renderItem(it, index): ?React.Element<any> {
	let label = it.label;
	let name = it.name;
	let type = it.type;
	let value = this.state[name] || it.value;
	let classNameParent = 'form-group ' + (it.classNameParent || '');
    return (
		<div className={classNameParent} key={index}>
			{ label &&
				<label>{it.label}
					{it.required ? <span className="required">(*)</span> : null}
				</label>
			}
			{ it.type &&
				<input
					type={type}
					name={name}
					className={it.className || 'form-control'}
					value={value}
					title={it.title || null}
					placeholder={it.placeholder || null}
					onChange={this.onChange}
					onClick={it.onClick || null}
					required={it.required ? true : null}
				/>
			}
		</div>
    );
  }

  // static propTypes = {
    // cookies: instanceOf(Cookies).isRequired
  // };
  // const { cookies } = this.props;
/*
  state = {
    pageType: 'Register',
    uLogin: self.localStorage.getItem('uLogin') || null,
    fName: self.localStorage.getItem('fName') || null,
    sName: self.localStorage.getItem('sName') || null,
    // tmpSess1: new Cookies().getAll() || false,
    // tmpSess: new Cookies().get('tmpSess') || false,
    isShowingModal: false
  }
*/
  handleChangeEmail = () => this.setState({isShowingModal: true, editProfile: false, pageType: 'ChangeEmail'})
  handleChangePass = () => this.setState({isShowingModal: true, editProfile: false, pageType: 'ChangePassword'})
  // handleProfile = () => this.setState({isShowingModal: true, editProfile: true, cmd: ''})
  handleLogout = () => {
	  this.reqUser({logout: 1, tmpSess: this.state.tmpSess});
	  this.setState({isShowingModal: false, tmpSess: false, cmd: ''});
  }
  handleLogin = () => this.setState({isShowingModal: true, pageType: 'Login', cmd: ''})
  handleRegister = () => this.setState({isShowingModal: true, pageType: 'Register', cmd: ''})
  handleClose = () => this.setState({isShowingModal: false, editProfile: false, cmd: ''})
  onCancel = (data) => this.setState(data)
  render() {
    let pageType = this.state.pageType || 'Register';
	let title = this.fieldsType[pageType].title || pageType;
	let validFields = this.getValidFields();
    let needCheckEmail = this.state.cmd === 'confirmEmail' ? true : false;
    let passSendEmail = this.state.cmd === 'passSendEmail' ? true : false;
    let errorLabel = this.state.err > 0 ? this.state.mess : false;
	
    let needForm = !this.state.cmd;
	if (this.state.editProfile) {
		needForm = false;
		title = 'Your profile';
	}
	// delete this.state.cmd;
// console.log('Register _______', this) // 
	let wrapClassName = 'center';
    return (
	<ul className="header-submenu pull-right">
		{ !this.state.tmpSess &&
			<li>
				<RegisterForm className="fa fa-users" appMessages={this.props.appMessages} onSubmit={this.reqUser} />
				<Login err={this.state.err} appMessages={this.props.appMessages} onSubmit={this.reqUser} onCancel={this.onCancel} />
			</li>
		}
		{ this.state.tmpSess &&
			<li>
				<div onClick={this.handleLogout} className="fa fa-sign-out"><span>{this.props.appMessages.logout}</span></div>
				<ProfileForm className="fa fa-users" appMessages={this.props.appMessages} onSubmit={this.postProfile} reqUser={this.reqUser} />
			</li>
		}
		{ this.state.isShowingModal &&
		  <Dialog
			title={title}
			wrapClassName={wrapClassName}
			animation="zoom"
			maskAnimation="fade"
			onClose={this.handleClose.bind(this)}
			visible>
			<div className="block-content-inner">
				<div className="row">
						{ errorLabel && <div className="errorLabel">{errorLabel}</div> }
						{ this.state.editProfile && <Profile handleChangeEmail={this.handleChangeEmail.bind(this)} handleChangePass={this.handleChangePass.bind(this)} /> }
					<div className="col-sm-4 col-sm-offset-4">
						{ passSendEmail && <div className="checkEmail">Please check your Mail box.</div> }
						{ needCheckEmail && <div className="checkEmail">Please check your Mail box and confirm Email.</div> }
						{ needForm && <form>{validFields.map(this.renderItem.bind(this))}</form> }
					</div>
				</div>
			</div>
		  </Dialog>
		}
	</ul>
	)
  }
}

export default connect(
  (store, ownProps) => ({
    user: store.user,
    ownProps
  }),
  // handleChangePass => {
	  // let rr = 12;
	  // this.setState({isShowingModal: true, pageType: 'ChangePassword'})
  // }
  // ,
  dispatch => ({
    onSubmitChange: (obj) => {
	  const data = { id: Date.now().toString(), obj };
	  dispatch({ type: 'USER_SUBMIT', data });
    }
  })
)(Register);
