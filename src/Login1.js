import React, {PropTypes} from 'react';
import { connect } from 'react-redux';

import { Button, Modal, Icon, Form, Input, Radio, Checkbox } from 'antd';
const FormItem = Form.Item;

function validateNumberAlpha(val) {
  if (val === 11) {
    return {
      validateStatus: 'success',
      errorMsg: null,
    };
  }
  return {
    validateStatus: 'error',
    errorMsg: 'The prime between 8 and 12 is 11!',
  };
}

const CollectionCreateForm = Form.create()(
  (props) => {
    const { visible, onCancel, onCreate, form, appMessages } = props;
    const { getFieldDecorator } = form;
	let handleNumberAlphaChange = (e) => {
console.log('handleNumberAlphaChange', e.target.value)
		let val = e.target.value;
		if (value.search(/^[\w]+$/) !== -1) {
		  return {
			validateStatus: 'error',
			errorMsg: 'The prime between 8 and 12 is 11!',
		  };
		}
		return {
		  validateStatus: 'success',
		  errorMsg: null,
		};
	};
    return (
      <Modal
        visible={visible}
        title={appMessages.login}
        okText={appMessages.log_in}
        onCancel={onCancel}
        onOk={onCreate}
      >
		<Form className="login-form">
			<FormItem>
			  {getFieldDecorator('uLogin', {
				rules: [{ required: true, message: 'Please input your username!' }],
			  })(
				<Input prefix={<Icon type="user" style={{ fontSize: 13 }} />} placeholder="Username" />
			  )}
			</FormItem>
			<FormItem
				validateStatus="error"
				help="Should be combination of numbers & alphabets"
			>
			  {getFieldDecorator('uPwd', {
				rules: [{ required: true, message: 'Please input your Password!' }],
			  })(
				<Input prefix={<Icon type="lock" style={{ fontSize: 13 }} />} type="password" placeholder="Password"
					onChange={handleNumberAlphaChange}
				/>
			  )}
			</FormItem>
			<FormItem>
			  {getFieldDecorator('remember', {
				valuePropName: 'checked',
				initialValue: true,
			  })(
				<Checkbox>Remember me</Checkbox>
			  )}
			  <div className="floatRight"><a className="login-form-forgot" href="">Forgot password</a>  or <a href="">Register now!</a></div>
			</FormItem>
        </Form>
      </Modal>
    );
  }
);

class Login extends React.Component {
  state = {
    visible: false,
  };
  showModal = () => {
    this.setState({ visible: true });
  }
  handleCancel = () => {
    this.setState({ visible: false });
  }
  handleCreate = () => {
    const form = this.form;
    form.validateFields((err, values) => {
      if (err) {
        return;
      }
      // console.log('Received values of form: ', values);
	  this.props.onSubmit(values);
      form.resetFields();
      this.setState({ visible: false });
    });
  }
  saveFormRef = (form) => {
    this.form = form;
  }
  render() {
	let appMessages = this.props.appMessages;
    return (
      <div>
		<div onClick={this.showModal} className="fa fa-sign-in">
			<span>{appMessages.login}</span>
		</div>
        <CollectionCreateForm
          ref={this.saveFormRef}
          appMessages={appMessages}
          visible={this.state.visible}
          onCancel={this.handleCancel}
          onCreate={this.handleCreate}
        />
      </div>
    );
  }
}

export default connect(
  (store, ownProps) => ({
    user: store.user,
    ownProps
  }),
  dispatch => ({
    onSubmitChange: (obj) => {
	  const data = { id: Date.now().toString(), obj };
	  dispatch({ type: 'USER_SUBMIT', data });
    }
  })
)(Login);
