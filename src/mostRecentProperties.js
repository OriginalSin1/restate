import React, { Component } from 'react';
import MostRecentList from '../src/components/MostRecentList';
// import load from '../src/utils/load';


export default class MostRecentProperties extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: null,
      active: 0,
      term: ''
    };

    this.loadData();
  }

  loadData() {
    fetch(this.props.data)
	  .then(response => { return response.json(); })
	  .then(agents => {
		  this.initialData = agents
		  this.setState({
			data: this.initialData
		  });
    });
  }

  updateData(config) {
    this.setState(config);
  }

  render() {
    return (
				<div className="block-content fullwidth background-white mostRecentProperties">
		<div className="block-content-inner">
            <MostRecentList data={this.state.data} update={this.updateData.bind(this)} />
        </div>
				</div>
    );
  }
}
