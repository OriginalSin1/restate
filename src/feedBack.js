import React, { Component } from 'react';
// import AgentList from '../src/components/AgentList';
// import load from '../src/utils/load';


export default class FeedBack extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: null,
      active: 0,
      term: ''
    };

    // this.loadData();
  }

  loadData() {
    fetch(this.props.data)
	  .then(response => { return response.json(); })
	  .then(agents => {
		  this.initialData = agents
		  this.setState({
			data: this.initialData
		  });
    });
  }

  updateData(config) {
    this.setState(config);
  }

  render() {
    return (
							<div className="feedBackContainer col-xs-12 col-sm-6 col-md-3">
        <div className="widget">
            <h3>Post Message</h3>

            <form method="post" action="?">
                <div className="form-group">
                    <input type="text" placeholder="E-mail" className="form-control" />
                </div>

                <div className="form-group">
                    <textarea className="form-control" placeholder="Message" rows="4"></textarea>
                </div>

                <div className="form-action">
                    <input type="submit" value="Send" className="btn btn-secondary pull-right" />
                </div>               
            </form>
        </div>
							</div>
    );
  }
}
