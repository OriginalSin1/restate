// import { SIGN_OUT } from 'redux-oauth';
// import { TIME_REQUEST_STARTED, TIME_REQUEST_FINISHED, TIME_REQUEST_ERROR } from 'redux/actions/timeActions';
// import enUS from 'antd/lib/locale-provider/en_US';
// import ruRU from 'antd/lib/locale-provider/ru_RU';

const initialState = {
  locale: self.localStorage.getItem('locale') || 'en',
  errors: null,
  auth: false
};

export default function (state = initialState, action) {
	// console.log('userReducer', action, state)
  switch (action.type) {
    case 'USER_SUBMIT':
    case 'USER_LOCALE':
      return Object.assign({}, state, action.data.obj);
    case 'PROFILE_SUBMIT':
      return Object.assign({}, state, action.data.obj);
    case 'USER_SIGN_OUT':
      return initialState;
    default:
      return state;
  }
}
