import { combineReducers } from 'redux';
// import { authStateReducer } from 'redux-oauth';
// import counterReducer from './counterReducer';
import userReducer from './userReducer';

export default combineReducers({
  // auth: authStateReducer,
  // counter: counterReducer,
  user: userReducer
});
