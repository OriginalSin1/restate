import React from 'react';

export default ({ user, update, index }) => {
  return (
	<a href="#" className="list-group-item">
		<i className="fa fa-angle-right"></i> {user.title} <span className="badge">{user.price}</span>
	</a>
  );
};
