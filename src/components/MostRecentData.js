import React from 'react';

export default ({ user, update, index }) => {
  return (
	<div className="col-sm-4 col-md-3">
		<div className="box image-zoom">
			<div className="box-picture">
				<div className="flag">{user.price}</div>
				<div className="box-content">
					<table className="box-table box-picture-meta">
						<tbody>
						<tr>
							<td>
								<strong>Area</strong><br/>
								{user.area}<sup>2</sup>
							</td>
							<td>
								<strong>Garage</strong><br/>
								{user.garage}
							</td>
							<td>
								<strong>Beds</strong><br/>
								{user.beds}
							</td>
							<td>
								<strong>Bath</strong><br/>
								{user.bath}
							</td>
						</tr>
						</tbody>
					</table>
				</div>

				<a href="#">
					<img src={user.img} alt="" />
				</a>
			</div>

			<div className="box-body">
				<h2 className="box-title-plain">
					<a href="#">{user.price}</a>
				</h2>

				<div className="box-location box-subtitle">
					<div className="location-country">{user.location}</div>
				</div>
			</div>
		</div>
	</div>
  );
};
