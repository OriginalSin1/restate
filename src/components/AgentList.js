import React from 'react';
import AgentData from './AgentData';

export default ({ data, update }) => {
  if (!data) { return (<p>Loading...</p>); }

  const users = data.map((user, index) => {
    return (<AgentData user={user} key={`user-${index}`} index={index} update={update} />);
  });

  return (
    <div className="block-content fullwidth background-gray">
		<div className="block-content-inner">
			<h3 className="widgetized-title">Our Agents</h3>
			<div className="row">
				{users}
			</div>
		</div>
	</div>
  );
};
