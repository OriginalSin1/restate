import React from 'react';

export default ({ user, update, index , curr}) => {
	// console.log('ssssss', index, curr)
  var className = 'box-image col-xs-12 col-sm-4 col-md-3 col-lg-2';
  if (curr !== 2) { className += ' hidden-xs'; }
  if (curr > 1 && curr < 5) { className += ' hidden-sm'; }
  if (curr === 0 && curr === 5) { className += ' hidden-md'; }

  return (
	<div className={className}>
		<a href="#">
			<img src={user.img} alt="" />

			<span className="flag">{user.price}</span>

			<span className="description">
				<span className="description-inner">
					<span className="title">{user.title}</span>
					<span className="row box-image-meta">
						<span className="box-image-meta-cell col-sm-3">
							<span className="box-image-meta-cell-inner">
								<strong>Area</strong>
								<span>{user.area}<sup>2</sup></span>
							</span>
						</span>

						<span className="box-image-meta-cell col-sm-3">
							<span className="box-image-meta-cell-inner">
								<strong>Garage</strong>
								<span>{user.garage}</span>
							</span>
						</span>

						<span className="box-image-meta-cell col-sm-3">
							<span className="box-image-meta-cell-inner">
								<strong>Beds</strong>
								<span>{user.beds}</span>
							</span>
						</span>

						<span className="box-image-meta-cell col-sm-3">
							<span className="box-image-meta-cell-inner">
								<strong>Bath</strong>
								<span>{user.bath}</span>
							</span>
						</span>
					</span>
				</span>
			</span>
		</a>
	</div>
  );
};
