import React from 'react';

export default ({ user, update, index }) => {
  return (
	<div className="col-sm-4">
		<div className="box background-white">
			<div className="row">
				<div className="col-md-12 col-lg-5" onClick={() => update({ active: index })}>
					<div className="row">
						<div className="box-picture">
							<a href="#">
								<img src={user.img} alt="">
								</img>
								<span></span>
							</a>
						</div>
					</div>
				</div>
				<div className="col-md-12 col-lg-7">
					<div className="row">
						<div className="box-body">
							<h2 className="box-title-plain">
								<a href="#">{user.name}</a>
							</h2>
							<p>{user.descr}</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
  );
};
