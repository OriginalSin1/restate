import React from 'react';

export default ({ user, update, index }) => {
  return (
	<div className="content-row-small row">
		<div className="content-row-small-picture col-xs-4 col-sm-4">
			<a href="#">
				<img src={user.img} alt="" />
				<span></span>
			</a>
		</div>

		<div className="content-row-small-body col-xs-8 col-sm-8">
			<h4><a href="#">{user.descr}</a></h4>
			<div className="content-row-small-meta">{user.name} / {user.date}</div>
		</div>
	</div>
  );
};
