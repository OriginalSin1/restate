import React from 'react';
import FeaturedData from './FeaturedData';

export default ({ data, update }) => {
  if (!data) { return (<p>Loading...</p>); }

  // const users = data.map((user, index) => {
    // return (<FeaturedData user={user} key={`user-${index}`} index={index} update={update} />);
  // });
  var list = [[], []],
	i, len;
  for (i = 0, len = Math.min(data.length, 16); i < len; i++) {
	var nm = i < 6 ? 0 : 1;
	// var it = (<FeaturedData user={data[i]} key={`user-${i}`} index={i} curr={list[nm].length} update={update} />);
	list[nm].push(<FeaturedData user={data[i]} key={`user-${i}`} index={i} curr={list[nm].length} update={update} />);
  }
	
  return (
	<div className="container">
		<h3 className="widgetized-title">Featured Properties</h3>
		<div className="box-images center-meta-on-hover clearfix">
			{list[0]}
		</div>
		<div className="box-images center-meta-on-hover clearfix">
			{list[1]}
		</div>
	</div>
  );
};
