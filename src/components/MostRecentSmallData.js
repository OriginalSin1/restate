import React from 'react';

export default ({ user, update, index }) => {
  return (
    <div className="box-image no-border col-xs-12 col-sm-4 col-md-3 col-lg-2 hidden-xs hidden-sm hidden-md">
        <a href="#">
            <img src={user.img} alt="" />

            <span className="flag">{user.price}</span>

            <span className="description">
                <span className="description-inner">
                    <span className="title">{user.title}</span>

                    <span className="box-image-meta row">
                        <span className="box-image-meta-cell col-xs-3 col-sm-3">
                            <span className="box-image-meta-cell-inner">
                                <strong>Area</strong>
                                <span>{user.area}<sup>2</sup></span>
                            </span>
                        </span>

                        <span className="box-image-meta-cell col-xs-3 col-sm-3">
                            <span className="box-image-meta-cell-inner">
                                <strong>Garage</strong>
                                <span>{user.garage}</span>
                            </span>
                        </span>

                        <span className="box-image-meta-cell col-xs-3 col-sm-3">
                            <span className="box-image-meta-cell-inner">
                                <strong>Beds</strong>
                                <span>{user.beds}</span>
                            </span>
                        </span>

                        <span className="box-image-meta-cell col-xs-3 col-sm-3">
                            <span className="box-image-meta-cell-inner">
                                <strong>Bath</strong>
                                <span>{user.bath}</span>
							</span>
						</span>
					</span>
				</span>
			</span>
		</a>
	</div>
  );
};
