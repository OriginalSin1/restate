import React from 'react';
import MostRecentSmallData from './MostRecentSmallData';

export default ({ data, update }) => {
  if (!data) { return (<p>Loading...</p>); }

  const users = data.map((user, index) => {
    return (<MostRecentSmallData user={user} key={`user-${index}`} index={index} update={update} />);
  });

  return (
	{users}
  );
};
