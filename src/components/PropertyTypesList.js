import React from 'react';
import PropertyTypesData from './PropertyTypesData';

export default ({ data, update }) => {
  if (!data) { return (<p>Loading...</p>); }

  const users = data.map((user, index) => {
    return (<PropertyTypesData user={user} key={`user-${index}`} index={index} update={update} />);
  });

  return (
	<div className="list-group">
		{users}
	</div>
  );
};
