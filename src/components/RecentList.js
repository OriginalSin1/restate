import React from 'react';
import RecentData from './RecentData';

export default ({ data, update }) => {
  if (!data) { return (<p>Loading...</p>); }

  const users = data.map((user, index) => {
    return (<RecentData user={user} key={`user-${index}`} index={index} update={update} />);
  });

  return (
	<div className="content-rows-small">
		{users}
	</div>
  );
};
