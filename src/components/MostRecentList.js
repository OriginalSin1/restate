import React from 'react';
import MostRecentData from './MostRecentData';

export default ({ data, update }) => {
  if (!data) { return (<p>Loading...</p>); }

  const users = data.map((user, index) => {
    return (<MostRecentData user={user} key={`user-${index}`} index={index} update={update} />);
  });

  return (
	<div className="boxes">
		<div className="row">
			<div className="col-sm-12">
				<h3 className="widgetized-title">Most Recent Properties</h3>
				{users}
			</div>
		</div>
	</div>
  );
};
