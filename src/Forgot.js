import React, {PropTypes} from 'react';
import { connect } from 'react-redux';

import { Button, Modal, Icon, Form, Input, Radio, Checkbox } from 'antd';
const FormItem = Form.Item;

const CollectionCreateForm = Form.create()(
  (props) => {
    const { visible, onCancel, onCreate, form, appMessages } = props;
    const { getFieldDecorator } = form;
    return (
      <Modal
        visible={visible}
        title={appMessages.forgot}
        okText={appMessages.send}
        onCancel={onCancel}
        onOk={onCreate}
      >
		<Form className="login-form">
			<FormItem hasFeedback>
			  {getFieldDecorator('uEmail', {
				rules: [{ type: 'email', required: true, message: appMessages.errors.email }],
			  })(
				<Input prefix={<Icon type="mail" style={{ fontSize: 13 }} />} placeholder={appMessages.inputs.uEmail} />
			  )}
			</FormItem>
        </Form>
      </Modal>
    );
  }
);

class Forgot extends React.Component {
  state = {
    visible: false,
  };
  showModal = () => {
    this.props.onCancel();
    this.setState({ visible: true });
  }
  handleCancel = () => {
    this.setState({ visible: false });
  }
  handleCreate = () => {
    const form = this.form;
    form.validateFields((err, values) => {
      if (err) {
        return;
      }
      console.log('Received values of form: ', values);
	  values.uForgot = 1;
	  this.props.onSubmit(values);
      form.resetFields();
      this.setState({ visible: false });
    });
  }
  saveFormRef = (form) => {
    this.form = form;
  }
  render() {
	let appMessages = this.props.appMessages;
    return (
		<span
			className="login-form-forgot"
			onClick={this.showModal}
		>{appMessages.forgot}
			<CollectionCreateForm
			  ref={this.saveFormRef}
			  appMessages={appMessages}
			  visible={this.state.visible}
			  onCancel={this.handleCancel}
			  onCreate={this.handleCreate}
			/>
		</span>
    );
  }
}

export default connect(
  (store, ownProps) => ({
    user: store.user,
    ownProps
  }),
  dispatch => ({
    onSubmitChange: (obj) => {
	  const data = { id: Date.now().toString(), obj };
	  dispatch({ type: 'USER_SUBMIT', data });
    }
  })
)(Forgot);
