import React, { Component } from 'react';
// import AgentList from '../src/components/AgentList';
// import load from '../src/utils/load';


export default class Technology extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: null,
      active: 0,
      term: ''
    };

    // this.loadData();
  }

  loadData() {
    fetch(this.props.data)
	  .then(response => { return response.json(); })
	  .then(agents => {
		  this.initialData = agents
		  this.setState({
			data: this.initialData
		  });
    });
  }

  updateData(config) {
    this.setState(config);
  }

  render() {
    return (
				<div className="technologyContainer block-content background-secondary background-technology fullwidth">
		<div className="block-content-inner">
			<div className="merged-boxes clearfix">
				<div className="merged-box col-sm-4">
					<div className="merged-box-content clearfix">
						<h3><i className="fa fa-windows color-windows"></i> Win Phone</h3>

						<p>
							Nullam in sodales metus, non scelerisque tellus. Quisque a volutpat nunc. Donec laoreet diam.
						</p>

						<a href="#" className="merged-box-read-more">Get your app <i className="fa fa-chevron-right"></i></a>
					</div>
				</div>
				<div className="merged-box col-sm-4">
					<div className="merged-box-content clearfix">
						<h3><i className="fa fa-apple color-apple"></i> iPhone</h3>

						<p>
							Nullam in sodales metus, non scelerisque tellus. Quisque a volutpat nunc. Donec laoreet diam.
						</p>

						<a href="#" className="merged-box-read-more">Get your app <i className="fa fa-chevron-right"></i></a>
					</div>
				</div>

				<div className="merged-box col-sm-4">

					<div className="merged-box-content clearfix">
						<h3><i className="fa fa-android color-android"></i> Android</h3>

						<p>
							Nullam in sodales metus, non scelerisque tellus. Quisque a volutpat nunc. Donec laoreet diam.
						</p>

						<a href="#" className="merged-box-read-more">Get your app <i className="fa fa-chevron-right"></i></a>
					</div>
				</div>
            </div>
        </div>
				</div>
    );
  }
}
