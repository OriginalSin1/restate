import React, { Component } from 'react';
import RecentList from '../src/components/RecentList';
// import load from '../src/utils/load';


export default class RecentProperties extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: null,
      active: 0,
      term: ''
    };

    this.loadData();
  }

  loadData() {
    fetch(this.props.data)
	  .then(response => { return response.json(); })
	  .then(agents => {
		  this.initialData = agents
		  this.setState({
			data: this.initialData
		  });
    });
  }

  updateData(config) {
    this.setState(config);
  }

  render() {
    return (
							<div className="recentProperties col-xs-12 col-sm-6 col-md-3">
		<div className="widget">
            <h3>Recent Properties</h3>
            <RecentList data={this.state.data} update={this.updateData.bind(this)} />
        </div>
							</div>
    );
  }
}
