import React, { Component } from 'react';
// import AgentList from '../src/components/AgentList';
// import load from '../src/utils/load';


export default class Social extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: null,
      active: 0,
      term: ''
    };

    // this.loadData();
  }

  loadData() {
    fetch(this.props.data)
	  .then(response => { return response.json(); })
	  .then(agents => {
		  this.initialData = agents
		  this.setState({
			data: this.initialData
		  });
    });
  }

  updateData(config) {
    this.setState(config);
  }

  render() {
    return (
		<div className="social block-content block-content-medium-padding fullwidth background-primary-dark">
		<div className="block-content-inner">
			<div className="social-stripe center">
				<a href="#" className="facebook">
					<i className="fa fa-facebook"></i>
				</a>
				<a href="#" className="twitter">
					<i className="fa fa-twitter"></i>
				</a>
				<a href="#" className="dribbble">
					<i className="fa fa-dribbble"></i>
				</a>
				<a href="#" className="linkedin">
					<i className="fa fa-linkedin"></i>
				</a>
				<a href="#" className="pinterest">
					<i className="fa fa-pinterest"></i>
				</a>
				<a href="#" className="dropbox">
					<i className="fa fa-dropbox"></i>
				</a>            
				<a href="#" className="linkedin">
					<i className="fa fa-linkedin"></i>
				</a>
				<a href="#" className="foursquare">
					<i className="fa fa-foursquare"></i>
				</a>            
				<a href="#" className="instagram">
					<i className="fa fa-instagram"></i>
				</a>                                    
			</div>
		</div>
						</div>
    );
  }
}
