import React      from 'react';
import ReactDOM   from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';


// import { Router } from 'react-router';
// import { Provider } from 'react-redux';
// import routes from './routes';
// import configureStore from './redux/configureStore';
// import createHistory from 'history/createBrowserHistory';
// import { syncHistoryWithStore } from 'react-router-redux';

import reducer from './redux/reducers';
import Main from '../src/main';

// const initialState = () => {
	// return window.REDUX_INITIAL_STATE || {
		// locale: 'enUS',
		// user: {
			// uLogin: ''
		// }
	// };
// }
const store = createStore(reducer, composeWithDevTools(applyMiddleware(thunk)));
// const store = createStore(reducer, composeWithDevTools(applyMiddleware(thunk)));
// const store = configureStore(initialState);
console.log('store', store.getState());
document.body.innerHTML = ''
var content = document.createElement('div');
ReactDOM.render(
	<Provider store={store}>
		<Main data="./assets/data/main.json" />
	</Provider>,
content);
document.body.append(content);

window._sss = store;
/*
 store={store}
    <Router history={history}>

import React from 'react';
import ReactDOM from 'react-dom';

document.body.innerHTML = ''
import Main from '../src/main';
var content = document.createElement('div');
ReactDOM.render(<Main data="./assets/data/main.json" />, content);
document.body.append(content);
console.log('ddd', process.env)

import React      from 'react';
import ReactDOM   from 'react-dom';
import { Router } from 'react-router';
import { Provider } from 'react-redux';
import routes from './routes';
import configureStore from './redux/configureStore';
// import createHistory from 'history/createBrowserHistory';
// import { syncHistoryWithStore } from 'react-router-redux';


const initialState = window.REDUX_INITIAL_STATE || { };

const store = configureStore(initialState);
// const history = syncHistoryWithStore(createHistory(), store);

const component = (
  <Provider store={store}>
    <Router>
      {routes(store)}
    </Router>
  </Provider>
);

document.body.innerHTML = ''
var content = document.createElement('div');
ReactDOM.render(component, content);
document.body.append(content);

*/

