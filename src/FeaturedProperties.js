import React, { Component } from 'react';
import FeaturedList from '../src/components/FeaturedList';


export default class FeaturedProperties extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: null,
      active: 0,
      term: ''
    };

    this.loadData();
  }

  loadData() {
    fetch(this.props.data)
	  .then(response => { return response.json(); })
	  .then(agents => {
		  this.initialData = agents
		  this.setState({
			data: this.initialData
		  });
    });
  }

  updateData(config) {
    this.setState(config);
  }

  render() {
    return (
				<div className="block-content no-padding-bottom featuredProperties">
		<div className="block-content-inner">
            <FeaturedList data={this.state.data} update={this.updateData.bind(this)} />
        </div>
				</div>
    );
  }
}
