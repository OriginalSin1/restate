import React, {PropTypes} from 'react';
import { connect } from 'react-redux';

import Forgot from './Forgot';
import RegisterForm from '../src/RegisterForm';
import { Button, Modal, Icon, Form, Input, Radio, Checkbox } from 'antd';
const FormItem = Form.Item;

const CollectionCreateForm = Form.create()(
  (props) => {
    const { visible, onCancel, onCreate, onSubmit, form, appMessages, err } = props;
    const { getFieldDecorator } = form;
	const pattern = /^[\w]+$/;
	let errMess = appMessages.errors[err] || '';

    return (
      <Modal
        visible={visible}
        title={appMessages.login}
        okText={appMessages.log_in}
        onCancel={onCancel}
        onOk={onCreate}
      >
		<div className="error-title">{errMess}</div>
		<Form className="login-form">
			<FormItem hasFeedback>
			  {getFieldDecorator('uLogin', {
				rules: [
					{ pattern: pattern, message: appMessages.errors.w },
					{ max: 20, message: appMessages.errors.len }
				],
			  })(
				<Input placeholder={appMessages.uLogin} prefix={<Icon type="user" style={{ fontSize: 13 }} />} />
			  )}
			</FormItem>
			<FormItem hasFeedback>
			  {getFieldDecorator('uPwd', {
				rules: [{ required: true, pattern: pattern, max: 10, message: appMessages.errors.w }],
			  })(
				<Input placeholder={appMessages.uPwd} type="password" prefix={<Icon type="lock" style={{ fontSize: 13 }} />} />
			  )}
			</FormItem>
			<FormItem>
			  {getFieldDecorator('remember', {
				valuePropName: 'checked',
				initialValue: true,
			  })(
				<Checkbox>Remember me</Checkbox>
			  )}
			  <div className="floatRight">
			  	<Forgot appMessages={appMessages} onSubmit={onSubmit} onCancel={onCancel} />
				or
			  	<RegisterForm appMessages={appMessages} onSubmit={onSubmit} onCancel={onCancel} />
			  </div>
			</FormItem>
        </Form>
      </Modal>
    );
  }
);

export default class Login extends React.Component {
  state = {
    visible: false,
  };
  showModal = () => {
    this.setState({ visible: true });
  }
  handleCancel = () => {
	this.props.onCancel({err: 0});
    this.setState({ visible: false });
  }
  handleCreate = () => {
    const form = this.form;
    form.validateFields((err, values) => {
      if (err) {
        return;
      }
	  values.uAttr = 1;
	  this.props.onSubmit(values);
      form.resetFields();
      this.handleCancel();
    });
  }
  saveFormRef = (form) => {
    this.form = form;
  }
  render() {
// console.log('Login__', this.props)
	let appMessages = this.props.appMessages;
	let visible = this.state.visible ||
		this.props.err === -2 ||
		this.props.err === 3 ||
		this.props.err === 2;
    return (
      <span>
		<div onClick={this.showModal} className="fa fa-sign-in">
			<span>{appMessages.login}</span>
		</div>
        <CollectionCreateForm
          ref={this.saveFormRef}
          appMessages={appMessages}
          visible={visible}
          err={this.props.err}
          onCancel={this.handleCancel}
          onCreate={this.handleCreate}
          onSubmit={this.props.onSubmit}
        />
      </span>
    );
  }
}
