var mapUtils = {
	_promises: {
		_sessionKeys: {},
		_maps: {}
	},
	_dataManagers: {},
	_waitCmdHash: {},

    getTileAttributes: function(prop) {
        var tileAttributeIndexes = {},
            tileAttributeTypes = {};
        if (prop.attributes) {
            var attrs = prop.attributes,
                attrTypes = prop.attrTypes || null;
            if (prop.identityField) { tileAttributeIndexes[prop.identityField] = 0; }
            for (var a = 0; a < attrs.length; a++) {
                var key = attrs[a];
                tileAttributeIndexes[key] = a + 1;
                tileAttributeTypes[key] = attrTypes ? attrTypes[a] : 'string';
            }
        }
        return {
            tileAttributeTypes: tileAttributeTypes,
            tileAttributeIndexes: tileAttributeIndexes
        };
    },

	extend: function (dest) { // (Object[, Object, ...]) ->
		var sources = Array.prototype.slice.call(arguments, 1),
		    i, j, len, src;

		for (j = 0, len = sources.length; j < len; j++) {
			src = sources[j] || {};
			for (i in src) {
				if (src.hasOwnProperty(i)) {
					dest[i] = src[i];
				}
			}
		}
		return dest;
	},

	setOptions: function (obj, options) {
		obj.options = mapUtils.extend({}, obj.options, options);
		return obj.options;
	},

    iterateNode: function(treeInfo, callback) {
        var iterate = function(node) {
			var arr = node.children,
				flag = false;
            for (var i = 0, len = arr.length; i < len; i++) {
                var layer = arr[i];

				callback(layer);
                if (layer.type === 'group') {
                    flag = iterate(layer.content);
                }
            }
			return flag;
        };

        treeInfo && iterate(treeInfo);
    },
    requestSessionKey: function(options) {
        var keys = mapUtils._promises._sessionKeys,
			serverHost = options.serverHost || defaults.serverHost;

        if (!(serverHost in keys)) {
            var apiKey = options.apiKey || defaults.apiKey;
            keys[serverHost] = new Promise(function(resolve, reject) {
				if (apiKey) {
					var url = defaults.protocol + '//' + serverHost + '/ApiKey.ashx?WrapStyle=None&Key=' + apiKey;
					mapUtils.requestJSON(url).then(function(response) {
						return response.json();
					}).then(function(json) {
						if (json && json.Status === 'ok') {
							resolve(json.Result.Key);
						} else {
							reject();
						}
					});
				} else {
					resolve('');
				}
			});
        }
        return keys[serverHost];
    },

    // parseResponse: function(response) {
		// var contentType = response.headers.get('content-type');
		// if(contentType && contentType.indexOf('application/json') !== -1) {
			// return response.json();
		// } else {
			// return '';
			// console.log('Oops, we haven`t got JSON from `' + url + '`!');
		// }
    // },

    requestJSON: function(url, data, options) {
		if (url) {
			options = options || {};
			if (data) {
				if (options.method === 'POST') {
					var formData  = new FormData();
					for(var name in data) { formData.append(name, data[name]); }
					options.body = formData;
				} else {
					url += (url.indexOf('?') === -1 ? '?' : '&') + Object.keys(data).reduce(function(p, k) {
						if (data[k]) { p.push(k + '=' + data[k]); }
						return p;
					}, []).join('&');
				}
			}

			return fetch(url, options);
		} else {
			console.log('requestJSON: bar URI `' + url + '`!');
		}
    },

	loadMapProperties: function(options) {
		var maps = mapUtils._promises._maps,
			serverHost = options.hostName || options.serverHost || defaults.serverHost,
			mapName = options.mapID;

        if (!maps[serverHost] || !maps[serverHost][mapName]) {
			var opt = {
				WrapStyle: 'None',
				skipTiles: options.skipTiles || 'None', // All, NotVisible, None
				MapName: mapName,
				srs: options.srs || '',	// 3857
				ModeKey: 'map'
			};
            maps[serverHost] = maps[serverHost] || {};
            maps[serverHost][mapName] = {
				promise: new Promise(function(resolve, reject) {
					mapUtils.requestSessionKey({serverHost: serverHost, apiKey: options.apiKey}).then(function(sessionKey) {
						opt.key = sessionKey;
						mapUtils.requestJSON(defaults.protocol + '//' + serverHost + '/TileSender.ashx', opt).then(function(response) {
							return response.json();
						}).then(function(json) {
							if (json && json.Status === 'ok' && json.Result) {
								json.Result.properties.hostName = serverHost;
								resolve(json.Result);
							} else {
								reject(json);
							}
						});
					});
				})
			};
        }
        return maps[serverHost][mapName].promise;
    },

	iterateMapTree: function(mapTree, options) {	// ???????? ?? ???????? ??????
		var maps = mapUtils._promises._maps,
			serverHost = options.hostName || options.serverHost || defaults.serverHost,
			serverHostPromises = maps[serverHost] || {};
			flag = false,
			promiseArr = [];

		mapUtils.iterateNode(mapTree, function(node) {
			var props = node.content.properties;
if (props.GroupID === 'BvuGm52gvxHt9RZp') {	// ???????
props.dataSource = 'T4CUM';
}
			var dataSource = props.dataSource || '';
			node.gmxOptions = {
				dataSource: dataSource,
				mapID: options.mapID
			};
			node.id = props.name || props.GroupID;
			node.text = props.title;
			node.children = node.content.children;
			if (node.type === 'group' && dataSource) {
				if (props.expanded || props.visible) {
					node.gmxOptions.dataSourceType = 'map';
					if (!serverHostPromises[dataSource]) {
						flag = true;
						promiseArr.push(new Promise(function(resolve) {
							var options1 = {mapID: dataSource, id: options.id};
							mapUtils.loadMapProperties(options1).then(function(subMapTree) {
								node.children = subMapTree.children;
								var promiseArr1 = mapUtils.iterateMapTree(subMapTree, options1);
								Promise.all(promiseArr1).then(resolve);
							});
						}));
					} else {
						serverHostPromises[dataSource].promise.then(function(subMapTree) {
							node.children = subMapTree.children;
						});
					} 

				} else {
					node.children = true;
				}
			} else if (node.type === 'layer' && props.visible) {
				mapUtils.createDataManager(node);
				if (!mapUtils._waitCmdHash[options.id].visible) { mapUtils._waitCmdHash[options.id].visible = []; }
				mapUtils._waitCmdHash[options.id].visible.push(node);
			}
		});
		return promiseArr;
    },

	createDataManager: function(node, clearVersion) {
		mapUtils._dataManagers[node.id] = new mapUtils.DataManager(node.content.properties, clearVersion);
    }
};
window.mapUtils = mapUtils;